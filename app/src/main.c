#include "stm8s.h"
#include "butt.h"


void number (uint32_t cislo)  
{
    if (cislo == 0)
    {
        GPIO_WriteLow(GPIOE, GPIO_PIN_1);  //seg-A
        GPIO_WriteLow(GPIOE, GPIO_PIN_2);  //seg-B
        GPIO_WriteLow(GPIOC, GPIO_PIN_5);  //seg-C
        GPIO_WriteLow(GPIOC, GPIO_PIN_7);  //seg-D
        GPIO_WriteLow(GPIOC, GPIO_PIN_6);  //seg-E
        GPIO_WriteLow(GPIOE, GPIO_PIN_5);  //seg-F
        GPIO_WriteHigh(GPIOC, GPIO_PIN_4);  //seg-G
    }
    else if (cislo == 1)
    {
        GPIO_WriteHigh(GPIOE, GPIO_PIN_1);
        GPIO_WriteLow(GPIOE, GPIO_PIN_2);
        GPIO_WriteLow(GPIOC, GPIO_PIN_5);
        GPIO_WriteHigh(GPIOC, GPIO_PIN_7);
        GPIO_WriteHigh(GPIOC, GPIO_PIN_6);
        GPIO_WriteHigh(GPIOE, GPIO_PIN_5);
        GPIO_WriteHigh(GPIOC, GPIO_PIN_4);
    }
    else if (cislo == 2)
    {
        GPIO_WriteLow(GPIOE, GPIO_PIN_1);
        GPIO_WriteLow(GPIOE, GPIO_PIN_2);
        GPIO_WriteHigh(GPIOC, GPIO_PIN_5);
        GPIO_WriteLow(GPIOC, GPIO_PIN_7);
        GPIO_WriteLow(GPIOC, GPIO_PIN_6);
        GPIO_WriteHigh(GPIOE, GPIO_PIN_5);
        GPIO_WriteLow(GPIOC, GPIO_PIN_4);
    }
    else if (cislo == 3)
    {
        GPIO_WriteLow(GPIOE, GPIO_PIN_1);
        GPIO_WriteLow(GPIOE, GPIO_PIN_2);
        GPIO_WriteLow(GPIOC, GPIO_PIN_5);
        GPIO_WriteLow(GPIOC, GPIO_PIN_7);
        GPIO_WriteHigh(GPIOC, GPIO_PIN_6);
        GPIO_WriteHigh(GPIOE, GPIO_PIN_5);
        GPIO_WriteLow(GPIOC, GPIO_PIN_4);
    }
    else if (cislo == 4)
    {
        GPIO_WriteHigh(GPIOE, GPIO_PIN_1);
        GPIO_WriteLow(GPIOE, GPIO_PIN_2);
        GPIO_WriteLow(GPIOC, GPIO_PIN_5);
        GPIO_WriteHigh(GPIOC, GPIO_PIN_7);
        GPIO_WriteHigh(GPIOC, GPIO_PIN_6);
        GPIO_WriteLow(GPIOE, GPIO_PIN_5);
        GPIO_WriteLow(GPIOC, GPIO_PIN_4);
    }
    else if (cislo == 5)
    {
        GPIO_WriteLow(GPIOE, GPIO_PIN_1);
        GPIO_WriteHigh(GPIOE, GPIO_PIN_2);
        GPIO_WriteLow(GPIOC, GPIO_PIN_5);
        GPIO_WriteLow(GPIOC, GPIO_PIN_7);
        GPIO_WriteHigh(GPIOC, GPIO_PIN_6);
        GPIO_WriteLow(GPIOE, GPIO_PIN_5);
        GPIO_WriteLow(GPIOC, GPIO_PIN_4);
    }
    else if (cislo == 6)
    {
        GPIO_WriteHigh(GPIOE, GPIO_PIN_1);
        GPIO_WriteHigh(GPIOE, GPIO_PIN_2);
        GPIO_WriteLow(GPIOC, GPIO_PIN_5);
        GPIO_WriteLow(GPIOC, GPIO_PIN_7);
        GPIO_WriteLow(GPIOC, GPIO_PIN_6);
        GPIO_WriteLow(GPIOE, GPIO_PIN_5);
        GPIO_WriteLow(GPIOC, GPIO_PIN_4);
    }
    else if (cislo == 7)
    {
        GPIO_WriteLow(GPIOE, GPIO_PIN_1);
        GPIO_WriteLow(GPIOE, GPIO_PIN_2);
        GPIO_WriteLow(GPIOC, GPIO_PIN_5);
        GPIO_WriteHigh(GPIOC, GPIO_PIN_7);
        GPIO_WriteHigh(GPIOC, GPIO_PIN_6);
        GPIO_WriteHigh(GPIOE, GPIO_PIN_5);
        GPIO_WriteHigh(GPIOC, GPIO_PIN_4);
    }
    else if (cislo == 8)
    {
        GPIO_WriteLow(GPIOE, GPIO_PIN_1);
        GPIO_WriteLow(GPIOE, GPIO_PIN_2);
        GPIO_WriteLow(GPIOC, GPIO_PIN_5);
        GPIO_WriteLow(GPIOC, GPIO_PIN_7);
        GPIO_WriteLow(GPIOC, GPIO_PIN_6);
        GPIO_WriteLow(GPIOE, GPIO_PIN_5);
        GPIO_WriteLow(GPIOC, GPIO_PIN_4);
    }
    else if (cislo == 9)
    {
        GPIO_WriteLow(GPIOE, GPIO_PIN_1);
        GPIO_WriteLow(GPIOE, GPIO_PIN_2);
        GPIO_WriteLow(GPIOC, GPIO_PIN_5);
        GPIO_WriteHigh(GPIOC, GPIO_PIN_7);
        GPIO_WriteHigh(GPIOC, GPIO_PIN_6);
        GPIO_WriteLow(GPIOE, GPIO_PIN_5);
        GPIO_WriteLow(GPIOC, GPIO_PIN_4);
    }
}

void main(void)
{
    CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1);
    GPIO_Init(GPIOC, GPIO_PIN_4, GPIO_MODE_OUT_PP_HIGH_SLOW);
    GPIO_Init(GPIOE, GPIO_PIN_5, GPIO_MODE_OUT_PP_HIGH_SLOW);
    GPIO_Init(GPIOC, GPIO_PIN_6, GPIO_MODE_OUT_PP_HIGH_SLOW);
    GPIO_Init(GPIOC, GPIO_PIN_7, GPIO_MODE_OUT_PP_HIGH_SLOW);
    GPIO_Init(GPIOC, GPIO_PIN_5, GPIO_MODE_OUT_PP_HIGH_SLOW);
    GPIO_Init(GPIOE, GPIO_PIN_2, GPIO_MODE_OUT_PP_HIGH_SLOW);
    GPIO_Init(GPIOE, GPIO_PIN_1, GPIO_MODE_OUT_PP_HIGH_SLOW);
    GPIO_Init(GPIOC, GPIO_PIN_3, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_Init(GPIOE, GPIO_PIN_4, GPIO_MODE_IN_FL_NO_IT);
    delay_timer_init();
    number(0);
    uint8_t bu=0;
    uint32_t blik=0;
    while (1)
    {
        if (button_is_press())
        {
            bu++;
            number(bu);
            for (uint64_t i=0; i<100000; i++)
            {
            }
            if (bu>9)
            {
                bu=0;
            }
        }
        else
        {
            blik=bu*100;
            delay_frek(blik);
        }
    }
}