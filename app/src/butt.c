#include "stm8s.h"
#include "butt.h"

uint8_t button_is_press(void)
{
    return (GPIO_ReadInputPin(GPIOE, GPIO_PIN_4) == 0); 
}
void delay_timer_init(void)
{
    TIM3_TimeBaseInit(TIM3_PRESCALER_256, 62499);
    TIM3_Cmd(ENABLE);
}

void delay_frek (uint8_t frekvenc)
{
    GPIO_Init(GPIOC, GPIO_PIN_3, GPIO_MODE_OUT_PP_LOW_SLOW);
    uint32_t time_ms = 1/frekvenc;
    TIM3_SetCounter(0);
    for(uint32_t ms=0; ms < time_ms; ms++)
    {
        while(TIM3_GetFlagStatus(TIM3_FLAG_UPDATE) != SET)
        {
        
        }
        TIM3_ClearFlag(TIM3_FLAG_UPDATE);
    }
    GPIO_WriteReverse(GPIOC, GPIO_PIN_3);
}
