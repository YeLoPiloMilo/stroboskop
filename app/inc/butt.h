#ifndef INC_BUTT_H
#define INC_BUTT_H
 
#include "stm8s.h"


void delay_frek(uint8_t frekvenc);
void delay_timer_init(void);
uint8_t button_is_press(void);

#endif