#<b>Stroboskop</b>


#FUNKCE
Je to zařízení, které bliká LED diodou rychlostí podle zadané frekvence. Funguje tak, že na stisk tlačítka se dá přepínat mezi 10 stavy a to jsou čísla 0-9, jenž se pak ukazují na 7-segmentovém displeji. Každý stav odpovídá své hodnotě frekvence a to tak, že vezme číslo stavu a vynasobí ho 100 (vásledná frekvence).

# BLOKOVÉ SCHÉMA

```mermaid
flowchart TB
    USB[PC]--+5V-->MCU[STM8]
    MCU--+3.3V-->seg[7-Segment]
    MCU--Data-->seg[7-segment]
    MCU--+3.3V-->LED[LED dioda]
```

# SCHÉMA ZAPOJENÍ

![Schema v programu KICAD](schema.png)

![Zapojeni na nepajivem poli](zapojeni.jpg)

<b>[Video zařízení v chodu](https://youtube.com/shorts/eNgXq4mfaAg?feature=share)</b>

# SEZNAM SOUČÁSTEK
| Typ součastky | Hodnota | Počet kusů |  Cena za kus |  Cena dohromady |
|:-------------:|:-------:|:----------:|:------------:|:---------------:|
| 7seg. displej |   ---   |      1     |      7Kč     |        7Kč      |
|    Rezistor   |   570R  |      8     |      2Kč     |        8Kč      |
|    Drátky     |   ---   |     50     |      1Kč     |       50Kč      |
| STM8 Nucleo   |   ---   |      1     |    250Kč     |      250Kč      |
| Celkem:       |   ---   |     ---    |     ---      |      315Kč      |


# ZÁVĚR

Při práci na projektu jsem se potýkal s hodně různými problémy, ale i když mi to kvuli nim mnohonasobne dele trvalo tak si myslím že jsem se tímto, možná ne tak příjemným, způsobem seznámil a naučil práci s těmito prostředími (hlavně s prostředím git) víc, než kdyby šlo vše hladce, takže svou práci hodnotím velice prospěšnou pro můj seberozvoj a znalosti a jsem s ní spokojen.
